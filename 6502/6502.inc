; 6502 instruction set.

include "modes.inc"
include "../utility/xcalm.inc"
include "../utility/once.inc"
once

iterate <name,opcode>, \
        BRK,0x00, CLC,0x18, CLD,0xD8, CLI,0x58, CLV,0xB8, \
        DEX,0xCA, DEY,0x88, INX,0xE8, INY,0xC8, NOP,0xEA, \
        PHA,0x48, PHP,0x08, PLA,0x68, PLP,0x28, RTI,0x40, \
        RTS,0x60, SEC,0x38, SED,0xF8, SEI,0x78, TAX,0xAA, \
        TAY,0xA8, TSX,0xBA, TXA,0x8A, TXS,0x9A, TYA,0x98
  calminstruction name?
    asm db opcode
  end calminstruction
end iterate

;             imp    imm    abs    abx    aby    iab    iax    izp    izx    izy    zp     zpx    zpy
iterate <name,blah>, \
        ADC,<( m65),(0x69),(0x6D),(0x7D),(0x79),( m65),( m65),( m65),(0x61),(0x71),(0x65),(0x75),( m65)>, \
        AND,<( m65),(0x29),(0x2D),(0x3D),(0x39),( m65),( m65),( m65),(0x21),(0x31),(0x25),(0x35),( m65)>, \
        ASL,<(0x0A),( m65),(0x0E),(0x1E),( m65),( m65),( m65),( m65),( m65),( m65),(0x06),(0x16),( m65)>, \
        BIT,<( m65),( m65),(0x2C),( m65),( m65),( m65),( m65),( m65),( m65),( m65),(0x24),( m65),( m65)>, \
        CMP,<( m65),(0xC9),(0xCD),(0xDD),(0xD9),( m65),( m65),( m65),(0xC1),(0xD1),(0xC5),(0xD5),( m65)>, \
        CPX,<( m65),(0xE0),(0xEC),( m65),( m65),( m65),( m65),( m65),( m65),( m65),(0xE4),( m65),( m65)>, \
        CPY,<( m65),(0xC0),(0xCC),( m65),( m65),( m65),( m65),( m65),( m65),( m65),(0xC4),( m65),( m65)>, \
        DEC,<( m65),( m65),(0xCE),(0xDE),( m65),( m65),( m65),( m65),( m65),( m65),(0xC6),(0xD6),( m65)>, \
        EOR,<( m65),(0x49),(0x4D),(0x5D),(0x59),( m65),( m65),( m65),(0x41),(0x51),(0x45),(0x55),( m65)>, \
        INC,<( m65),( m65),(0xEE),(0xFE),( m65),( m65),( m65),( m65),( m65),( m65),(0xE6),(0xF6),( m65)>, \
        JMP,<( m65),( m65),(0x4C),( m65),( m65),(0x6C),( m65),( m65),( m65),( m65),( m65),( m65),( m65)>, \
        JSR,<( m65),( m65),(0x20),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65)>, \
        LDA,<( m65),(0xA9),(0xAD),(0xBD),(0xB9),( m65),( m65),( m65),(0xA1),(0xB1),(0xA5),(0xB5),( m65)>, \
        LDX,<( m65),(0xA2),(0xAE),( m65),(0xBE),( m65),( m65),( m65),( m65),( m65),(0xA6),( m65),(0xB6)>, \
        LDY,<( m65),(0xA0),(0xAC),(0xBC),( m65),( m65),( m65),( m65),( m65),( m65),(0xA4),(0xB4),( m65)>, \
        LSR,<(0x4A),( m65),(0x4E),(0x5E),( m65),( m65),( m65),( m65),( m65),( m65),(0x46),(0x56),( m65)>, \
        ORA,<( m65),(0x09),(0x0D),(0x1D),(0x19),( m65),( m65),( m65),(0x01),(0x11),(0x05),(0x15),( m65)>, \
        ROL,<(0x2A),( m65),(0x2E),(0x3E),( m65),( m65),( m65),( m65),( m65),( m65),(0x26),(0x36),( m65)>, \
        ROR,<(0x6A),( m65),(0x6E),(0x7E),( m65),( m65),( m65),( m65),( m65),( m65),(0x66),(0x76),( m65)>, \
        SBC,<( m65),(0xE9),(0xED),(0xFD),(0xF9),( m65),( m65),( m65),(0xE1),(0xF1),(0xE5),(0xF5),( m65)>, \
        STA,<( m65),( m65),(0x8D),(0x9D),(0x99),( m65),( m65),( m65),(0x81),(0x91),(0x85),(0x95),( m65)>, \
        STX,<( m65),( m65),(0x8E),( m65),( m65),( m65),( m65),( m65),( m65),( m65),(0x86),( m65),(0x96)>, \
        STY,<( m65),( m65),(0x8C),( m65),( m65),( m65),( m65),( m65),( m65),( m65),(0x84),(0x94),( m65)>
  calminstruction name? opr&
    xcall m65.operand, opr, blah
  end calminstruction
end iterate

iterate <name,opcode,inverse>, \
        BCC,0x90,0xB0, BCS,0xB0,0x90, BEQ,0xF0,0xD0, BMI,0x30,0x10, \
        BNE,0xD0,0xF0, BPL,0x10,0x30, BVC,0x50,0x70, BVS,0x70,0x50
  calminstruction name? target*
    xcall m65.branch, (opcode), (inverse), target, (m65)
  end calminstruction
end iterate

end once
