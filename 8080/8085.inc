; 8085 instruction set.

include "8080.inc"
include "../utility/xcalm.inc"
include "../utility/once.inc"
once

iterate <name,opcoden>, dsub,010q, arhl,020q, rdel,030q, rim,040q, sim,060q, shlx,331q, lhlx,355q
  calminstruction name?
    asm db opcoden
  end calminstruction
end iterate

iterate <name,opcoden>, ldhi,050q, ldsi,070q
  calminstruction name? imm*
    asm db opcoden
    local buffer
    asmarranged buffer, =db imm
  end calminstruction
end iterate

iterate <name,opcoden>, jnk,335q, jk,375q
  calminstruction name? imm*
    asm db opcoden
    local buffer
    asmarranged buffer, =dw imm
  end calminstruction
end iterate

end once
