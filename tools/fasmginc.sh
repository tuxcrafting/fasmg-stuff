#!/bin/sh

IFS="\n"

echo "$1"
sed 's/^\s*include\s\+['"'"'"]\(.\+\)['"'"'"]\s*$/\1/; t; d' "$1" | while read -r p
do
    p="$(dirname "$1")/$p"
    "$0" "$p"
done
