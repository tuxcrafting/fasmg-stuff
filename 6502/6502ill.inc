include "6502.inc"
include "../utility/xcalm.inc"
include "../utility/once.inc"
once

iterate <name,opcode>, \
        JAM,0x02
  calminstruction name?
    asm db opcode
  end calminstruction
end iterate

;             imp    imm    abs    abx    aby    iab    iax    izp    izx    izy    zp     zpx    zpy
iterate <name,blah>, \
        ALR,<( m65),(0x4B),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65)>, \
        ANC,<( m65),(0x0B),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65)>, \
        ANE,<( m65),(0x8B),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65)>, \
        ARR,<( m65),(0x6B),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65)>, \
        DCP,<( m65),( m65),(0xCF),(0xDF),(0xDB),( m65),( m65),( m65),(0xC3),(0xD3),(0xC7),(0xD7),( m65)>, \
        ISC,<( m65),( m65),(0xEF),(0xFF),(0xFB),( m65),( m65),( m65),(0xE3),(0xF3),(0xE7),(0xF7),( m65)>, \
        LAS,<( m65),( m65),( m65),( m65),(0xBB),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65)>, \
        LAX,<( m65),( m65),(0xAF),( m65),(0xBF),( m65),( m65),( m65),(0xA3),(0xB3),(0xA7),( m65),(0xB7)>, \
        LXA,<( m65),(0xAB),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65)>, \
        RLA,<( m65),( m65),(0x2F),(0x3F),(0x3B),( m65),( m65),( m65),(0x23),(0x33),(0x27),(0x37),( m65)>, \
        RRA,<( m65),( m65),(0x6F),(0x7F),(0x7B),( m65),( m65),( m65),(0x63),(0x73),(0x67),(0x77),( m65)>, \
        SAX,<( m65),( m65),(0x8F),( m65),( m65),( m65),( m65),( m65),(0x83),( m65),(0x87),( m65),(0x97)>, \
        SBX,<( m65),(0xCB),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65)>, \
        SHA,<( m65),( m65),( m65),( m65),(0x9F),( m65),( m65),( m65),( m65),(0x93),( m65),( m65),( m65)>, \
        SHX,<( m65),( m65),( m65),( m65),(0x9E),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65)>, \
        SHY,<( m65),( m65),( m65),(0x9C),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65)>, \
        SLO,<( m65),( m65),(0x0F),(0x1F),(0x1B),( m65),( m65),( m65),(0x03),(0x13),(0x07),(0x17),( m65)>, \
        SRE,<( m65),( m65),(0x4f),(0x5F),(0x5B),( m65),( m65),( m65),(0x43),(0x53),(0x47),(0x57),( m65)>, \
        TAS,<( m65),( m65),( m65),( m65),(0x9B),( m65),( m65),( m65),( m65),( m65),( m65),( m65),( m65)>
  calminstruction name? opr&
    xcall m65.operand, opr, blah
  end calminstruction
end iterate

end once
