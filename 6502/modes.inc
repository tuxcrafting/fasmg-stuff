include "../utility/xcalm.inc"
include "../utility/once.inc"
once

element m65

calminstruction m65.operand opr*, imp*, imm*, abs*, abx*, aby*, iab*, iax*, izp*, izx*, izy*, zp*, zpx*, zpy*
  local buffer

  match =A?, opr
  jno skip_imp
  check imp eq m65
  jyes inv_imp
  asmarranged buffer, =db imp
  exit
inv_imp:
  asm err "invalid implied"
skip_imp:

  match #opr, opr
  jno skip_imm
  check imm eq m65
  jyes inv_imm
  match <opr, opr
  jno not_lsb
  compute opr, (opr shr 0) and 255
  jump not_msb
not_lsb:
  match >opr, opr
  jno not_msb
  compute opr, (opr shr 8) and 255
not_msb:
  asmarranged buffer, =db imm
  asmarranged buffer, =db opr
  exit
inv_imm:
  asm err "invalid immediate"
skip_imm:

  match ( opr =, =X? ), opr
  jno skip_inx
  check iax eq m65 & izx eq m65
  jyes inv_inx
  check ~(izx eq m65) & opr >= 0x00 & opr <= 0xFF
  jno force_iax
  asmarranged buffer, =db izx
  asmarranged buffer, =db opr
  exit
force_iax:
  check iax eq m65
  jyes inv_inx
  asmarranged buffer, =db iax
  asmarranged buffer, =dw opr
  exit
inv_inx:
  asm err "invalid indirect x"
skip_inx:

  match ( opr ) =, Y?, opr
  jno skip_iny
  check izy eq m65
  jyes inv_iny
  asmarranged buffer, =db izy
  asmarranged buffer, =db opr
  exit
inv_iny:
  asm err "invalid indirect y"
skip_iny:

  match ( opr ), opr
  jno skip_ind
  check iab eq m65 & izp eq m65
  jyes inv_ind
  check ~(izp eq m65) & opr >= 00 & opr <= 0xFF
  jno force_iab
  asmarranged buffer, =db izp
  asmarranged buffer, =db opr
  exit
force_iab:
  check iab eq m65
  jyes inv_ind
  asmarranged buffer, =db iab
  asmarranged buffer, =dw opr
  exit
inv_ind:
  asm err "invalid indirect"
skip_ind:

  match opr =, =X?, opr
  jno skip_x
  check abx eq m65 & zpx eq m65
  jyes inv_x
  check ~(zpx eq m65) & opr >= 0x00 & opr <= 0xFF
  jno force_abx
  asmarranged buffer, =db zpx
  asmarranged buffer, =db opr
  exit
force_abx:
  check abx eq m65
  jyes inv_x
  asmarranged buffer, =db abx
  asmarranged buffer, =dw opr
  exit
inv_x:
  asm err "invalid x"
skip_x:

  match opr =, =Y?, opr
  jno skip_y
  check aby eq m65 & zpy eq m65
  jyes inv_y
  check ~(zpy eq m65) & opr >= 0x00 & opr <= 0xFF
  jno force_aby
  asmarranged buffer, =db zpy
  asmarranged buffer, =db opr
  exit
force_aby:
  check aby eq m65
  jyes inv_y
  asmarranged buffer, =db aby
  asmarranged buffer, =dw opr
  exit
inv_y:
  asm err "invalid y"
skip_y:

  compute opr, opr
  check ~(zp eq m65) & opr >= 0x00 & opr <= 0xFF
  jno force_abs
  asmarranged buffer, =db zp
  asmarranged buffer, =db opr
  exit
force_abs:
  check abs eq m65
  jyes inv_abs
  asmarranged buffer, =db abs
  asmarranged buffer, =dw opr
  exit
inv_abs:
  asm err "invalid absolute"
end calminstruction

ALLOW_LONG_BRANCHES = 0

calminstruction m65.branch opcode*, inverse*, target*, opr1*
  local disp
  compute disp, target - $ - 2
  check opr1 eq m65
  jyes d_no_opr1
  compute disp, disp - 1
d_no_opr1:
  compute disp, disp and 0xFF
  check ALLOW_LONG_BRANCHES <> 0 & ~(inverse eq m65) & (disp < -128 | disp >= 128)
  jyes long

  local buffer
  asmarranged buffer, =db opcode
  check opr1 eq m65
  jyes no_opr1
  asmarranged buffer, =db opr1
no_opr1:
  asmarranged buffer, =db disp
  exit

long:
  asmarranged buffer, =db inverse
  check opr1 eq m65
  jyes l_no_opr1
  asmarranged buffer, =db opr1
l_no_opr1:
  asm db 0x03, 0x4C
  asmarranged buffer, =dw target
end calminstruction

end once
