include "../utility/xcalm.inc"
include "../utility/once.inc"
once

define i8008 i8008

macro i8008.mk0 name*, opcode*
  local lopcode
  lopcode = opcode
  calminstruction name?
    local buffer
    asmarranged buffer, =db lopcode
  end calminstruction
end macro

macro i8008.mk1 name*, opcode*
  local lopcode
  lopcode = opcode
  calminstruction name? imm*
    local buffer
    compute imm, imm
    compute imm, +imm
    asmarranged buffer, =db lopcode, imm
  end calminstruction
end macro

macro i8008.mk2 name*, opcode*
  local lopcode
  lopcode = opcode
  calminstruction name? imm*
    local buffer
    compute imm, imm
    compute imm, +imm
    asmarranged buffer, =db lopcode
    asmarranged buffer, =dw imm
  end calminstruction
end macro

macro i8008.mk3 name*, opcode*, lb*, ub*, shift*
  local lopcode, llb, lub, lshift
  lopcode = opcode
  llb = lb
  lub = ub
  lshift = shift
  calminstruction name? opr*
    local buffer
    check opr >= llb & opr <= lub
    jyes cont
    asm err "invalid operand"
  cont:
    compute opr, opr shl lshift or lopcode
    asmarranged buffer, =db opr
  end calminstruction
end macro

iterate <r1,n1>, A,0, B,1, C,2, D,3, E,4, H,5, L,6, M,7
  i8008.mk1 L#r1#I, 0x06 + n1 * 8
  if n1 <> 0 & n1 <> 7
    i8008.mk0 IN#r1, 0x00 + n1 * 8
    i8008.mk0 DC#r1, 0x01 + n1 * 8
  end if
  iterate <r0,n0>, A,0, B,1, C,2, D,3, E,4, H,5, L,6, M,7
    if (n1 <> 0 | n0 <> 0) & (n1 <> 7 | n0 <> 7)
      i8008.mk0 L#r1#r0, 0xC0 + n1 * 8 + n0
    end if
  end iterate
end iterate

iterate <q,n>, AD,0, AC,1, SU,2, SB,3, ND,4, XR,5, OR,6, CP,7
  i8008.mk1 q#I, 0x04 + n * 8
  iterate <r,nr>, A,0, B,1, C,2, D,3, E,4, H,5, L,6, M,7
    i8008.mk0 q#r, 0x80 + n * 8 + nr
  end iterate
end iterate

i8008.mk0 RLC, 0x02
i8008.mk0 RRC, 0x0A

i8008.mk0 RAL, 0x12
i8008.mk0 RAR, 0x1A

i8008.mk0 RET, 0x07
i8008.mk2 JMP, 0x44
i8008.mk2 CAL, 0x46

iterate <c,n>, FC,0, FZ,1, FS,2, FP,3, TC,4, TZ,5, TS,6, TP,7
  i8008.mk0 R#c, 0x03 + n * 8
  i8008.mk2 J#c, 0x40 + n * 8
  i8008.mk2 C#c, 0x42 + n * 8
end iterate

i8008.mk0 NOP, 0xC0
i8008.mk0 HLT, 0xFF

i8008.mk3 RST, 0x05, 0, 7, 3
i8008.mk3 INP, 0x41, 0, 7, 1
i8008.mk3 OUT, 0x41, 8, 31, 1

end once
