; Defines a macro "once" .. "end once" which assembles once per file.
; Encompass entire .inc files to prevent them from being assembled more than once.

if ~(definite _once_inc_)
_once_inc_ := 1

calminstruction once?
  local ln, filen, sym
  compute filen, __file__ + 0
  arrange sym, =once#filen
  arrange ln, =if ~(=definite sym)
  assemble ln
  arrange ln, sym :== 1
  assemble ln
end calminstruction

macro end?.once?!
  end if
end macro

end if
