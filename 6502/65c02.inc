include "6502.inc"
include "../utility/xcalm.inc"
include "../utility/once.inc"
once

iterate <name,opcode>, \
        PHX,0xDA, PHY,0x5A, PLX,0xFA, PLY,0x7A, \
        STP,0xDB, WAI,0xCB
  calminstruction name?
    asm db opcode
  end calminstruction
end iterate

;             imp    imm    abs    abx    aby    iab    iax    izp    izx    izy    zp     zpx    zpy
iterate <name,blah>, \
        ADC,<( m65),(0x69),(0x6D),(0x7D),(0x79),( m65),( m65),(0x72),(0x61),(0x71),(0x65),(0x75),( m65)>, \
        AND,<( m65),(0x29),(0x2D),(0x3D),(0x39),( m65),( m65),(0x32),(0x21),(0x31),(0x25),(0x35),( m65)>, \
        BIT,<( m65),(0x89),(0x2C),(0x3C),( m65),( m65),( m65),( m65),( m65),( m65),(0x24),(0x34),( m65)>, \
        CMP,<( m65),(0xC9),(0xCD),(0xDD),(0xD9),( m65),( m65),(0xD2),(0xC1),(0xD1),(0xC5),(0xD5),( m65)>, \
        DEC,<(0x3A),( m65),(0xCE),(0xDE),( m65),( m65),( m65),( m65),( m65),( m65),(0xC6),(0xD6),( m65)>, \
        EOR,<( m65),(0x49),(0x4D),(0x5D),(0x59),( m65),( m65),(0x52),(0x41),(0x51),(0x45),(0x55),( m65)>, \
        INC,<(0x1A),( m65),(0xEE),(0xFE),( m65),( m65),( m65),( m65),( m65),( m65),(0xE6),(0xF6),( m65)>, \
        JMP,<( m65),( m65),(0x4C),( m65),( m65),(0x6C),(0x7C),( m65),( m65),( m65),( m65),( m65),( m65)>, \
        LDA,<( m65),(0xA9),(0xAD),(0xBD),(0xB9),( m65),( m65),(0xB2),(0xA1),(0xB1),(0xA5),(0xB5),( m65)>, \
        ORA,<( m65),(0x09),(0x0D),(0x1D),(0x19),( m65),( m65),(0x12),(0x01),(0x11),(0x05),(0x15),( m65)>, \
        SBC,<( m65),(0xE9),(0xED),(0xFD),(0xF9),( m65),( m65),(0xF2),(0xE1),(0xF1),(0xE5),(0xF5),( m65)>, \
        STA,<( m65),( m65),(0x8D),(0x9D),(0x99),( m65),( m65),(0x92),(0x81),(0x91),(0x85),(0x95),( m65)>, \
        STZ,<( m65),( m65),(0x9C),(0x9E),( m65),( m65),( m65),( m65),( m65),( m65),(0x64),(0x74),( m65)>, \
        TRB,<( m65),( m65),(0x1C),( m65),( m65),( m65),( m65),( m65),( m65),( m65),(0x14),( m65),( m65)>, \
        TRB,<( m65),( m65),(0x0C),( m65),( m65),( m65),( m65),( m65),( m65),( m65),(0x04),( m65),( m65)>
  calminstruction name? opr&
    xcall m65.operand, opr, blah
  end calminstruction
end iterate

calminstruction BRA? target*
  xcall m65.branch, (0x80), (m65), target, (m65)
end calminstruction

iterate <name,opcodeb,inverseb>, \
        BBR,0x0F,0x8F, BBS,0x8F,0x0F
  calminstruction name? bit*, addr*, target*
    check bit >= 0 & bit <= 7
    jno error
    local opcode, inverse
    compute opcode, opcodeb or bit shl 4
    compute inverse, inverseb or bit shl 4
    xcall m65.branch, opcode, inverse, target, addr
    exit
  error:
    asm err "invalid bit position"
  end calminstruction
end iterate

iterate <name,opcodeb>, \
        RMB,0x07, SMB,0x87
  calminstruction name? bit*, addr*
    check bit >= 0 & bit <= 7
    jno error
    local opcode
    compute opcode, opcodeb or bit shl 4
    local buffer
    asmarranged buffer, =db opcode, addr
    exit
  error:
    asm err "invalid bit position"
  end calminstruction
end iterate

end once
