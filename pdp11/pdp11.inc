; PDP-11 instruction set.

include "../utility/xcalm.inc"
include "../utility/once.inc"
once

define pdp11 pdp11

element pdp11.r

repeat 8, i : 0
  element R#i? : pdp11.r + i
end repeat

PC? := R7
SP? := R6

pdp11.instr_base = 0

calminstruction pdp11.init_instr
  compute pdp11.instr_base, $
  compute pdp11.opr1_has_index, 0
  compute pdp11.opr1_has_imm, 0
  compute pdp11.opr1_relative, 0
  compute pdp11.opr2_has_index, 0
  compute pdp11.opr2_has_imm, 0
  compute pdp11.opr2_relative, 0
end calminstruction

repeat 2, i : 1
  pdp11.opr#i#_regmode = 0
  pdp11.opr#i#_has_index = 0
  pdp11.opr#i#_index = 0
  pdp11.opr#i#_has_imm = 0
  pdp11.opr#i#_imm = 0
  pdp11.opr#i#_relative = 0

  calminstruction pdp11.do_opr#i opr*
    local mode, reg, index

    compute mode, 2
    match =# opr, opr
    jyes immediate

    compute mode, 3
    match =@ =# opr, opr
    jyes immediate

    compute mode, 3
    match =@(reg)+, opr
    jyes cont1

    compute mode, 5
    match =@-(reg), opr
    jyes cont1

    compute mode, 2
    match (reg)+, opr
    jyes cont1

    compute mode, 4
    match -(reg), opr
    jyes cont1

    compute mode, 7
    match =@ index(reg), opr
    jyes cont1

    compute mode, 6
    match index(reg), opr
    jyes cont1

    match =@ index, opr
    jyes relative7

    compute mode, 1
    match (reg), opr
    jyes cont1

    compute mode, 0
    compute reg, opr

  cont1:
    compute reg, reg
    check elementsof reg = 0 & (mode = 0 | mode = 1)
    jyes relative6

    check elementsof reg = 1 & reg scale 0 = 0 & reg scale 1 = 1 & reg metadata 1 relativeto pdp11.r
    jno invalid_reg
    compute reg, reg metadata 1 scale 0

  cont2:
    check mode = 6 | mode = 7
    jno no_index
    compute pdp11.opr#i#_has_index, 1
    compute pdp11.opr#i#_index, index
  no_index:
    compute pdp11.opr#i#_regmode, mode shl 3 or reg
    exit

  relative6:
    compute pdp11.opr#i#_relative, 1
    compute index, reg
    compute mode, 6
    compute reg, 7
    jump cont2

  relative7:
    compute pdp11.opr#i#_relative, 1
    compute mode, 7
    compute reg, 7
    jump cont2

  immediate:
    compute reg, 7
    compute pdp11.opr#i#_has_imm, 1
    compute pdp11.opr#i#_imm, opr
    jump cont2

  invalid_reg:
    asm err "invalid register"
  end calminstruction

  calminstruction pdp11.opr#i#_do_index
    check pdp11.opr#i#_has_index = 1
    jno no

    local value
    compute value, pdp11.opr#i#_index
    check pdp11.opr#i#_relative = 1
    jno indexed

    compute value, value - (pdp11.instr_base + 1 + pdp11.opr1_has_index + pdp11.opr2_has_index)
    compute value, value and 0xFFFF

  indexed:
    local buffer
    asmarranged buffer, =dw value
  no:
  end calminstruction
end repeat

calminstruction pdp11.opr_do_imm
  check pdp11.opr1_has_imm = 1 & pdp11.opr2_has_imm = 1
  jyes too_many
  check pdp11.opr1_has_imm = 1
  jyes opr1
  check pdp11.opr2_has_imm = 1
  jno fin
opr2:
  local buffer, value
  compute value, pdp11.opr2_imm and 0xFFFF
  asmarranged buffer, =dw value
  exit
opr1:
  compute value, pdp11.opr1_imm and 0xFFFF
  asmarranged buffer, =dw value
fin:
  exit
too_many:
  asm err "too many immediates"
end calminstruction

pdp11.regn = 0
calminstruction pdp11.get_reg reg*
  check elementsof reg = 1 & reg scale 0 = 0 & reg scale 1 = 1 & reg metadata 1 relativeto pdp11.r
  jno invalid_reg
  compute pdp11.regn, reg metadata 1 scale 0
  exit
invalid_reg:
  asm err "invalid register"
end calminstruction

; double-operand
iterate <opcode,name>, \
        01q,MOV, 11q,MOVB, 02q,CMP, 12q,CMPB, \
        03q,BIT, 13q,BITB, 04q,BIC, 14q,BICB, \
        05q,BIS, 15q,BISB, 06q,ADD, 16q,SUB
  calminstruction name? src*, dest*
    local base
    init base, opcode shl 12
    xcall pdp11.init_instr
    xcall pdp11.do_opr1, src
    xcall pdp11.do_opr2, dest
    local buffer, instr
    compute instr, base or pdp11.opr1_regmode shl 6 or pdp11.opr2_regmode
    asmarranged buffer, =dw instr
    xcall pdp11.opr1_do_index
    xcall pdp11.opr2_do_index
    xcall pdp11.opr_do_imm
  end calminstruction
end iterate

; reg-mem
iterate <opcode,name,src,dest>, \
        070q,MUL,mem,reg, \
        071q,DIV,mem,reg, \
        072q,ASH,mem,reg, \
        073q,ASHC,mem,reg, \
        074q,XOR,reg,mem, \
        004q,JSR,reg,mem
  calminstruction name? src, dest
    local base
    init base, opcode shl 9
    xcall pdp11.init_instr
    xcall pdp11.do_opr1, mem
    xcall pdp11.get_reg, reg
    local buffer, instr
    compute instr, base or pdp11.regn shl 6 or pdp11.opr1_regmode
    asmarranged buffer, =dw instr
    xcall pdp11.opr1_do_index
    xcall pdp11.opr_do_imm
  end calminstruction
end iterate

; single operand
iterate <opcode,name>, \
        0001q,JMP, \
        0003q,SWAB, \
        0050q,CLR, 1050q,CLRB, \
        0051q,COM, 1051q,COMB, \
        0052q,INC, 1052q,INCB, \
        0053q,DEC, 1053q,DECB, \
        0054q,NEG, 1054q,NEGB, \
        0055q,ADC, 1055q,ADCB, \
        0056q,SBC, 1056q,SBCB, \
        0057q,TST, 1057q,TSTB, \
        0060q,ROR, 1060q,RORB, \
        0061q,ROL, 1061q,ROLB, \
        0062q,ASR, 1062q,ASRB, \
        0063q,ASL, 1063q,ASLB, \
        1064q,MTPS, \
        0065q,MFPI, 1065q,MFPD, \
        0066q,MTPI, 1066q,MTPD, \
        0067q,SXT, 1067q,MFPS
  calminstruction name? opr*
    local base
    init base, opcode shl 6
    xcall pdp11.init_instr
    xcall pdp11.do_opr1, opr
    local buffer, instr
    compute instr, base or pdp11.opr1_regmode
    asmarranged buffer, =dw instr
    xcall pdp11.opr1_do_index
    xcall pdp11.opr_do_imm
  end calminstruction
end iterate

iterate <opcode,c,name>, \
        000q,1,BR, \
        001q,0,BNE, 001q,1,BEQ, \
        002q,0,BGE, 002q,1,BLT, \
        003q,0,BGT, 003q,1,BLE, \
        100q,0,BPL, 100q,1,BMI, \
        101q,0,BHI, 101q,1,BLOS, \
        102q,0,BVC, 102q,1,BVS, \
        103q,0,BCC, 103q,1,BCS, \
        103q,0,BHIS, 103q,1,BLO
  calminstruction name? target*
    local base
    init base, opcode shl 9 or c shl 8
    compute target, (target - $ - 2) / 2
    check target >= -128 & target < 128
    jno invalid_range
    local buffer, instr
    compute instr, base or (target and 0xFF)
    asmarranged buffer, =dw instr
    exit
  invalid_range:
    asm err "invalid branch range"
  end calminstruction
end iterate

calminstruction SOB? reg*, target*
  xcall pdp11.get_reg, reg
  compute target, ($ + 2 - target) / 2
  check target >= -32 & target < 32
  jno invalid_range
  local buffer, instr
  compute instr, 077000q or pdp11.regn shl 6 or (target and 63)
  asmarranged buffer, =dw instr
  exit
invalid_range:
  asm err "invalid branch range"
end calminstruction

calminstruction RTS? reg*
  xcall pdp11.get_reg, reg
  local buffer, instr
  compute instr, 000200q or pdp11.regn
  asmarranged buffer, =dw instr
end calminstruction

calminstruction MARK? n*
  check n >= 0 & n < 64
  jno invalid_range
  local buffer, instr
  compute instr, 006400q or n
  asmarranged buffer, =dw instr
  exit
invalid_range:
  asm err "invalid value range"
end calminstruction

iterate <s,name>, 0,EMT, 1,TRAP
  calminstruction name? code*
    local base
    init base, 104000q or s shl 8
    check code >= 0 & code < 256
    jno invalid_code
    local buffer, instr
    compute instr, base or code
    asmarranged buffer, =dw instr
    exit
  invalid_code:
    asm err "invalid code"
  end calminstruction
end iterate

; no operand
iterate <opcode,name>,
        000000q,HALT,
        000001q,WAIT,
        000002q,RTI,
        000003q,BPT,
        000004q,IOT,
        000005q,RESET,
        000006q,RTT
  macro name?
    dw opcode
  end macro
end iterate

; condition code

iterate <s,name>, 0,CL, 1,SE
  calminstruction name? flags&
    local base
    init base, 000240q or s shl 4
    local buffer, instr
    compute instr, base

  begin:
    match , flags
    jyes fin
    local flag
    match flag =, flags, flags
    jyes do_flag
    arrange flag, flags
    arrange flags,

  do_flag:
    local bit
    compute bit, 1
    match =C?, flag
    jyes add_bit
    compute bit, 2
    match =V?, flag
    jyes add_bit
    compute bit, 4
    match =Z?, flag
    jyes add_bit
    compute bit, 8
    match =N?, flag
    jyes add_bit
    asm err "invalid flag"

  add_bit:
    compute instr, instr or bit
    jump begin

  fin:
    asmarranged buffer, =dw instr
  end calminstruction
end iterate

iterate <name,instr>, \
        CLC,<CL C>, \
        CLV,<CL V>, \
        CLZ,<CL Z>, \
        CLN,<CL N>, \
        CCC,<CL C, V, Z, N>, \
        SEC,<SE C>, \
        SEV,<SE V>, \
        SEZ,<SE Z>, \
        SEN,<SE N>, \
        SCC,<SE C, V, Z, N>
  macro name?
    instr
  end macro
end iterate

end once
