; Defines a set of anonymous labels, @@ and @0..@9.
;
; @b and @0b..@9b (or r instead of b) give the definition before, @f and
; @0f..@9f is the definition after.
;
; Should be used on a line by itself, otherwise in a line like "@@: instr", the
; values of @b and @f are the same (address of the instruction).

include "once.inc"
once

; Create a new anonymous label with the specified name and prefix.
macro @INIT name*, prefix*
  macro name tail&
    match label, prefix#f?
      label:
    end match

    prefix#b? equ prefix#f?
    prefix#r? equ prefix#f?

    local tmp
    tmp tail

    local anon
    prefix#f? equ anon
  end macro

  define prefix#f?
  name:
end macro

@INIT @@, @

repeat 10, i : 0
  @INIT @#i, @#i
end repeat

end once
