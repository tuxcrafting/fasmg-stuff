; 8080 instruction set.

include "../utility/xcalm.inc"
include "../utility/once.inc"
once

define i8080 i8080

i8080.r1 := 1 shl 0
i8080.sp1 := 1 shl 1
i8080.psw1 := 1 shl 2

element i8080.rbase
element i8080.r : i8080.rbase + i8080.r1
element i8080.sp : i8080.rbase + i8080.sp1
element i8080.psw : i8080.rbase + i8080.psw1
element i8080.rp : i8080.rbase + (i8080.r1 or i8080.sp1 or i8080.psw1)

element b? : i8080.rp + 0
element c? : i8080.r + 1
element d? : i8080.rp + 2
element e? : i8080.r + 3
element h? : i8080.rp + 4
element l? : i8080.r + 5
element m? : i8080.r + 6
element a? : i8080.r + 7
element sp? : i8080.sp + 6
element psw? : i8080.psw + 6

i8080.rn = 0

calminstruction i8080.check_r reg*, type*
  check elementsof reg = 1 & reg scale 0 = 0 & reg scale 1 = 1
  jno invreg

  compute reg, reg metadata 1
  check elementsof reg = 1 & reg scale 1 = 1
  jno invreg

  compute i8080.rn, reg scale 0

  compute reg, reg metadata 1
  check reg relativeto i8080.rbase & reg scale 0 and type
  jno invreg

  exit

invreg:
  asm err "invalid register"
end calminstruction

calminstruction mov? dest, src
  local opcode
  compute opcode, 100q
  xcall i8080.check_r, dest, i8080.r1
  compute opcode, opcode or i8080.rn shl 0
  xcall i8080.check_r, src, i8080.r1
  compute opcode, opcode or i8080.rn shl 3

  local buffer
  asmarranged buffer, =db opcode
end calminstruction

calminstruction rst? imm*
  compute imm, imm
  check imm >= 0 & imm <= 7
  jno range

  compute imm, 307q or imm shl 3
  local buffer
  asmarranged buffer, =db imm
  exit

range:
  asm err "invalid operand"
end calminstruction

calminstruction mvi? reg*, imm*
  local opcode
  xcall i8080.check_r, reg, i8080.r1
  compute opcode, 006q or i8080.rn shl 3

  local buffer
  asmarranged buffer, =db opcode
  asmarranged buffer, =db imm
end calminstruction

calminstruction lxi? reg*, imm*
  local opcode
  xcall i8080.check_r, reg, i8080.sp1
  compute opcode, 001q or i8080.rn shl 3

  local buffer
  asmarranged buffer, =db opcode
  asmarranged buffer, =dw imm
end calminstruction

iterate <name,opcoden>, stax,002q, ldax,012q
  calminstruction name? reg*
    local opcode
    check reg eq b | reg eq d
    compute opcode, opcoden or reg metadata 1 scale 0 shl 3

    local buffer
    asmarranged buffer, =db opcode
  end calminstruction
end iterate

iterate <name,opcoden>, \
        nop,000q, hlt,176q, ret,311q, xthl,343q, pchl,351q, xchg,353q, di,363q, sphl,371q, ei,373q, \
        rlc,007q, rrc,017q, ral,027q, rar,037q, daa,047q, cma,057q, stc,067q, cmc,077q, \
        rnz,300q, rz,310q, rnc,320q, rc,330q, rpo,340q, rpe,350q, rp,360q, rm,370q
  calminstruction name?
    asm db opcoden
  end calminstruction
end iterate

iterate <name,opcoden>, \
        shld,042q, lhld,052q, sta,062q, lda,072q, jmp,303q, call,305q, \
        jnz,302q, jz,312q, jnc,322q, jc,332q, jpo,342q, jpe,352q, jp,362q, jm,372q, \
        cnz,304q, cz,314q, cnc,324q, cc,334q, cpo,344q, cpe,354q, cp,364q, cm,374q
  calminstruction name? imm*
    asm db opcoden
    local buffer
    asmarranged buffer, =dw imm
  end calminstruction
end iterate

iterate <name,opcoden>, \
        out,323q, in,333q, \
        adi,306q, aci,316q, sui,326q, sbi,336q, ani,346q, xri,356q, ori,366q, cpi,376q
  calminstruction name? imm*
    asm db opcoden
    local buffer
    asmarranged buffer, =db imm
  end calminstruction
end iterate

iterate <name,opcoden,shift>, \
        inr,004q,3, dcr,005q,3, \
        add,200q,0, adc,210q,0, sub,220q,0, sbb,230q,0, ana,240q,0, xra,250q,0, ora,260q,0, cmp,270q,0
  calminstruction name? reg*
    local opcode
    xcall i8080.check_r, reg, i8080.r1
    compute opcode, opcoden or i8080.rn shl shift

    local buffer
    asmarranged buffer, =db opcode
  end calminstruction
end iterate

iterate <name,opcoden,type>, \
        dad,012q,sp1, inx,003q,sp1, dcx,013q,sp1, \
        pop,301q,psw1, push,305q,psw1
  calminstruction name? reg*
    local opcode
    xcall i8080.check_r, reg, i8080.type
    compute opcode, opcoden or i8080.rn shl 3

    local buffer
    asmarranged buffer, =db opcode
  end calminstruction
end iterate

end once
