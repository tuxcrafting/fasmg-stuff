; Utilities for writing CALM code.

include "once.inc"
once

; Give an initial numeric value to a variable.
calminstruction calminstruction?.init? var*, val : 0
  compute val, val
  publish var, val
end calminstruction

; Give an initial symbolic value to a variable.
calminstruction calminstruction?.initsym? var*, val&
  publish var, val
end calminstruction

; Generate a new identifier and store it in a variable.
; Uses the variable name as a prefix.
calminstruction calminstruction?.unique? name*
  local counter, buffer
  init counter
  compute counter, counter + 1
  arrange buffer, name#counter
  publish name, buffer
end calminstruction

; Generate code to assemble a line as is.
calminstruction calminstruction?.asm? line&
  local tmp, ln, buffer
  initsym tmp, unique ln
  assemble tmp
  publish ln, line
  arrange buffer, =assemble ln
  assemble buffer
end calminstruction

; Like call, but allows constants: numeric between (), symbolic between <>.
calminstruction calminstruction?.xcall? instr*, args&
  arrange instr, =call instr

process_args:
  match , args
  jyes fin

  local arg
  match arg=,args, args
  jyes argument

  arrange arg, args
  arrange args,

argument:
  match (arg), arg
  jyes numeric
  match <arg>, arg
  jyes symbolic

  arrange instr, instr,arg
  jump process_args

numeric:
  compute arg, arg

symbolic:
  local name
  asm unique name
  publish name, arg

  arrange instr, instr,name
  jump process_args

fin:
  assemble instr
end calminstruction

; Sequence of "arrange" and "assemble".
macro calminstruction?.asmarranged? variable*, pattern&
  arrange variable, pattern
  assemble variable
end macro

end once
